class CreateCardcollections < ActiveRecord::Migration
  def change
    create_table :cardcollections do |t|
      t.string :name
      t.string :category
      t.text :description
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
