class CreateCards < ActiveRecord::Migration
  def change
    create_table :cards do |t|
      t.string :name
      t.string :knowledge
      t.text :description
      t.references :cardcollection, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
