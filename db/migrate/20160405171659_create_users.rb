class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :username
      t.string :country
      t.string :language

      t.timestamps null: false
    end
  end
end
